# Variables file
# Sets devault values or references terraform.tfvars 
variable "aws_account_id" {
  type        = string
  description = "AWS account ID"
}

variable "aws_region" {
  type        = string
  description = "Provider region"
}

variable "vpc_cidr" {
  default     = "17.1.0.0/25"
}

variable "subnets_cidr" {
  type        = list
  default     = ["17.1.0.0/27", "17.1.0.32/27"] 
}

variable "availability_zone" {
  type        = list
  #default     = ["us-east-1a", "us-east-1b"]
}

variable "webservers_ami" {
  type        = string
}

variable "instance_type" {
  default = "t3a.nano"
}

