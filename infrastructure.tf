# VPC definiton
resource "aws_vpc" "brett_vpc" {
  cidr_block       = var.vpc_cidr
  enable_dns_hostnames = true

  tags = {
    Name = "Brett VPC"
  }
}

# Subnets (public)
resource "aws_subnet" "public" {
  count = length(var.subnets_cidr)
  vpc_id = aws_vpc.brett_vpc.id
  cidr_block = element(var.subnets_cidr,count.index)
  availability_zone = element(var.availability_zone,count.index)
  map_public_ip_on_launch = true
  tags = {
    Name = "BrettSubnet${count.index+1}"
  }
}

# Internet Gateway
resource "aws_internet_gateway" "brett_igw" {
  vpc_id = aws_vpc.brett_vpc.id
  tags = {
    Name = "BrettInternetGateway"
  }
}

# Public Route Table
resource "aws_route_table" "public_rt" {
    vpc_id = aws_vpc.brett_vpc.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.brett_igw.id
    }
    tags = {
        Name = "BrettPublicRouteTable"
    }
}

# Route table association with public subnets
resource "aws_route_table_association" "rta" {
  count = length(var.subnets_cidr)
  subnet_id      = element(aws_subnet.public.*.id,count.index)
  route_table_id = aws_route_table.public_rt.id
}

# Security Groups for EC2 instances
resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Allow HTTP inbound connections"
  vpc_id = aws_vpc.brett_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Allow HTTP Security Group"
  }
}

# Launch Configuration
# Could update to launch template
resource "aws_launch_configuration" "web" {
  name_prefix = "web-"

  image_id = var.webservers_ami
  instance_type = var.instance_type

  security_groups = [ aws_security_group.allow_http.id ]
  associate_public_ip_address = true

  # Easier to update functionality in external file
  user_data = file("install_httpd.sh")

  lifecycle {
    create_before_destroy = true
  }
}

# Security Groups for ELB
resource "aws_security_group" "elb_http" {
  name        = "elb_http"
  description = "Allow HTTP traffic to instances through Elastic Load Balancer"
  vpc_id = aws_vpc.brett_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Allow HTTP through ELB Security Group"
  }
}

# ELB Elastic (Classic) Load Balancer
resource "aws_elb" "brett_elb" {
  name = "brett-elb"
  security_groups = [
    aws_security_group.elb_http.id
  ]
  subnets = aws_subnet.public.*.id

  cross_zone_load_balancing   = true

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    # Was "HTTP:80/" at some point
    target              = "HTTP:80/index.html"
    interval            = 30
  }

  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = "80"
    instance_protocol = "http"
  }

}

# Auto-Scaling Group
# Set the desired number on instances here
resource "aws_autoscaling_group" "web" {
  name = "${aws_launch_configuration.web.name}-asg"

  min_size             = 2
  desired_capacity     = 5
  max_size             = 7
  
  health_check_type    = "ELB"
  load_balancers = [
    aws_elb.brett_elb.id
  ]

  launch_configuration = aws_launch_configuration.web.name

  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]

  metrics_granularity = "1Minute"

  vpc_zone_identifier  = aws_subnet.public.*.id

  # Required to redeploy without an outage.
  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "web"
    propagate_at_launch = true
  }

}

# Not useful in our instance.
# Would tear down images since so few resources were used

#resource "aws_autoscaling_policy" "web_policy_up" {
#  name = "web_policy_up"
#  scaling_adjustment = 1
#  adjustment_type = "ChangeInCapacity"
#  cooldown = 300
#  autoscaling_group_name = aws_autoscaling_group.web.name
#}

#resource "aws_cloudwatch_metric_alarm" "web_cpu_alarm_up" {
#  alarm_name = "web_cpu_alarm_up"
#  comparison_operator = "GreaterThanOrEqualToThreshold"
#  evaluation_periods = "2"
#  metric_name = "CPUUtilization"
#  namespace = "AWS/EC2"
#  period = "120"
#  statistic = "Average"
#  threshold = "60"
#
#  dimensions = {
#    AutoScalingGroupName = aws_autoscaling_group.web.name
#  }
#
#  alarm_description = "This metric monitor EC2 instance CPU utilization"
#  alarm_actions = [ aws_autoscaling_policy.web_policy_up.arn ]
#}

#resource "aws_autoscaling_policy" "web_policy_down" {
#  name = "web_policy_down"
#  scaling_adjustment = -1
#  adjustment_type = "ChangeInCapacity"
#  cooldown = 300
#  autoscaling_group_name = aws_autoscaling_group.web.name
#}

#resource "aws_cloudwatch_metric_alarm" "web_cpu_alarm_down" {
#  alarm_name = "web_cpu_alarm_down"
#  comparison_operator = "LessThanOrEqualToThreshold"
#  evaluation_periods = "2"
#  metric_name = "CPUUtilization"
#  namespace = "AWS/EC2"
#  period = "120"
#  statistic = "Average"
#  threshold = "10"
#
#  dimensions = {
#    AutoScalingGroupName = aws_autoscaling_group.web.name
#  }
#
#  alarm_description = "This metric monitor EC2 instance CPU utilization"
#  alarm_actions = [ aws_autoscaling_policy.web_policy_down.arn ]
#}

output "brett_elb_name" {
  value = aws_elb.brett_elb.dns_name
}
