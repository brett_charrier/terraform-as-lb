!!use http instead of https!!
## Terraform Auto-Scaling Load-Balancer

## Description
This project uses Terraform to deploy 2 AWS EC2 machines on a VPC with 2 subnets behind a Load Balancer and a Auto-Scaling Group that increases the number of instances to 5.

## Installation
Make sure Terraform CLI and AWS CLI are installed
Run 'aws configure' and add local environment variables
Alter the terraform.tfvars file with the desired 'aws-account-id' and 'aws_region'
The 'availability_zone' and 'webservers_ami' are both dependent on the 'aws_region'

## Usage
terraform init
terraform plan
terraform apply
terraform destroy

Use http:// instead of https:// when navigating to EC2 or ELB URL

I created 'ami-02d44a12aa5f44405' so please contact me if it does not work

## Project status
Complete with optimizations possible

