#! /bin/bash
sudo yum update
sudo yum install -y httpd
sudo chkconfig httpd on
sudo service httpd start

EC2_INSTANCE_ID=`curl -s http://169.254.169.254/latest/meta-data/instance-id`
EC2_ID="`echo \"$EC2_INSTANCE_ID\" | sed 's/[a-z]$//'`"
echo "<h1>The machine name (AWS instance id) is $EC2_ID</h1>" | sudo tee /var/www/html/index.html
